package SegregateBowlOfFruits_Solution1;
import java.util.ArrayList;
import java.util.List;

// A fruit bowl contains fruits - Apples, Oranges and Bananas. Segregate them into different bowls

public class Solution {

	public static void main(String[] arg) {
		String[] bowlOfFruits = { "Apple", "Orange", "Banana", "Banana", "Orange", "Orange", "Apple", "Banana",
				"Orange" };
		System.out.print("Bowl of fruits: ");
		for(String fruit : bowlOfFruits) {
			System.out.print(fruit + " ");
		}
		System.out.println("\nBowl of fruits contain " + bowlOfFruits.length + " fruits.\n");
		
		List<String> bowlOfApples = new ArrayList<String>();
		List<String> bowlOfOranges = new ArrayList<String>();
		List<String> bowlOfBananas = new ArrayList<String>();

		for (int i = 0; i < bowlOfFruits.length; i++) {
			switch (bowlOfFruits[i]) {
			case "Apple":
				bowlOfApples.add(bowlOfFruits[i]);
				break;

			case "Orange":
				bowlOfOranges.add(bowlOfFruits[i]);
				break;

			case "Banana":
				bowlOfBananas.add(bowlOfFruits[i]);
				break;

			default:
			}
		}

		System.out.print("Bowl of apples: " + bowlOfApples + "\n" + "Bowl of apples contain " + bowlOfApples.size()
				+ " apples.\n\n");
		System.out.print("Bowl of oranges: " + bowlOfOranges + "\n" + "Bowl of oranges contain " + bowlOfOranges.size()
				+ " oranges.\n\n");
		System.out.print("Bowl of bananas: " + bowlOfBananas + "\n" + "Bowl of bananas contain " + bowlOfBananas.size()
				+ " bananas.");
	}
}
