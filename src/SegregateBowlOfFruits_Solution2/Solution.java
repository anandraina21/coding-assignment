package SegregateBowlOfFruits_Solution2;

//A fruit bowl contains fruits - Apples, Oranges and Bananas. Segregate them into different bowls

public class Solution {
	public static void main(String[] args) {
		BowlOfFruits bowlOfFruits = new BowlOfFruits();
		BowlOfFruits bowlOfApples = new BowlOfApples();
		BowlOfFruits bowlOfOranges = new BowlOfOranges();
		BowlOfFruits bowlOfBananas = new BowlOfBananas();
		
		bowlOfFruits.bowlOfFruit();
		bowlOfApples.bowlOfFruit();
		bowlOfOranges.bowlOfFruit();
		bowlOfBananas.bowlOfFruit();
	}
}
